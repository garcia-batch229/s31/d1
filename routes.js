/*
const http = require("http");

http.createServer(function(request,response){

    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end("Hello From our First NodeJS Server!");

}).listen(4000);



console.log("Serever is Running on localhost:4000!"); */

const http = require('http');

const port = 3000;

const server = http.createServer((req,res) => {
    if(req.url == "/greeting"){
        res.writeHead(200,{"Content-Type": "text/plain"});
        res.end("Hello World");
    } else if(req.url = "/homepage"){
         res.writeHead(200,{"Content-Type": "text/plain"});
        res.end("This is the homepage");
    }else {
         res.writeHead(404,{"Content-Type": "text/plain"});
        res.end("404: Page not found");
    }

})

server.listen(port);

console.log(`Edit:Server now accessible at localhost:${port}`);